﻿using System;

namespace FabAccessAPI.Exceptions
{
    /// <summary>
    /// Connecting to a server has failed
    /// InnerException will provide more information
    /// </summary>
    public class ConnectionException : Exception
    {
        public ConnectionException()
        {

        }

        public ConnectionException(string message) : base(message)
        {

        }

        public ConnectionException(string message, Exception inner) : base(message, inner)
        {

        }
    }
}
