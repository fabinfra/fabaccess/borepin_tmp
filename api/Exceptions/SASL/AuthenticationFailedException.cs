﻿using System;

namespace FabAccessAPI.Exceptions.SASL
{
    public class AuthenticationFailedException : Exception
    {
        #region Constructors
        public AuthenticationFailedException(byte[] additionalData = null)
        {
            AdditionalData = additionalData;
        }

        public AuthenticationFailedException(byte[] additionalData, string message) : base(message)
        {
            AdditionalData = additionalData;
        }

        public AuthenticationFailedException(byte[] additionalData, string message, Exception inner) : base(message, inner)
        {
            AdditionalData = additionalData;
        }
        #endregion

        #region Fields
        public byte[]? AdditionalData { get; }
        #endregion
    }
}
