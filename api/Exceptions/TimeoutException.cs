﻿using System;

namespace FabAccessAPI.Exceptions
{
    /// <summary>
    /// Timeout on Connection
    /// </summary>
    public class TimeoutException : Exception
    {
        public TimeoutException()
        {

        }

        public TimeoutException(string message) : base(message)
        {

        }

        public TimeoutException(string message, Exception inner) : base(message, inner)
        {

        }
    }
}
