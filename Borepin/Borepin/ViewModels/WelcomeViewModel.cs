﻿using AvaloniaInside.Shell;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Borepin.ViewModels
{
    public class WelcomeViewModel : ViewModelBase
    {
        private readonly INavigator _navigationService;
        public ICommand OpenCommand { get; set; }

        public WelcomeViewModel(INavigator navigationService)
        {
            _navigationService = navigationService;
            OpenCommand = ReactiveCommand.CreateFromTask(_OpenAsync);
        }

        private Task _OpenAsync(CancellationToken cancellationToken)
        {
            return _navigationService.NavigateAsync("/main", cancellationToken);
        }
    }

}
