using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using AvaloniaInside.Shell;
using ReactiveUI;

namespace Borepin.ViewModels
{
    public class ProfilViewModel : ViewModelBase
    {
        private readonly INavigator _navigationService;

        public string URI
        {
            get
            {
                return _navigationService.CurrentUri.ToString();

            }
        }
        public ProfilViewModel(INavigator navigationService)
        {
            _navigationService = navigationService;
        }
    }
}
