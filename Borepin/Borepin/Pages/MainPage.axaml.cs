using System;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.Styling;

namespace Borepin.Pages
{
    public partial class MainPage : TabControl, IStyleable
    {
        public Type StyleKey => typeof(TabControl);

        public MainPage()
        {
            _InitializeComponent();
        }

        private void _InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }

}
