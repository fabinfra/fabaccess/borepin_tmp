using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using AvaloniaInside.Shell;

namespace Borepin.Pages
{
    public partial class ProfilePage : Page
    {
        public ProfilePage()
        {
            _InitializeComponent();
        }

        private void _InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }

        public static string Icon => "fa-solid fa-user";
    }

}
