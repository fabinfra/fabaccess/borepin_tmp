using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using AvaloniaInside.Shell;
using Borepin.ViewModels;
using Borepin.Views;
using System.Threading;
using System.Threading.Tasks;

namespace Borepin.Pages
{
    public partial class SettingsPage : Page
    {
        public SettingsPage()
        {
            _InitializeComponent();
        }

        private void _InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }

        public static string Icon => "fa-solid fa-user";
    }

}
