using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using AvaloniaInside.Shell;
using System.Threading;
using System.Threading.Tasks;

namespace Borepin.Pages
{
    public partial class UsersPage : Page
    {
        public UsersPage()
        {
            _InitializeComponent();
        }

        private void _InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }

        public static string Icon => "fa-solid fa-house";
    }
}
