﻿using Avalonia.Controls;
using Avalonia.VisualTree;
using AvaloniaInside.Shell.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Borepin
{
    public static class Helper
    {
#pragma warning disable IDE0060 // Remove unused parameter
        public static void SetCarouselToTabControl(Carousel carousel, bool v)
        {
            var tabControl = carousel.FindAncestorOfType<TabControl>();
            if (tabControl == null)
            {
                carousel.AttachedToVisualTree += delegate { SetCarouselToTabControl(carousel, true); };
                return;
            }

            carousel.ItemsSource = tabControl.Items;
        }
    }
#pragma warning restore IDE0060 // Remove unused parameter
}
