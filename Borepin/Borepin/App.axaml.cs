using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using Projektanker.Icons.Avalonia;
using Projektanker.Icons.Avalonia.FontAwesome;
using Borepin.ViewModels;
using Borepin.Views;
using ReactiveUI;
using Splat;
using Borepin.Pages;

namespace Borepin
{
    public partial class App : Application
    {
        public override void Initialize()
        {
            IconProvider.Current
               .Register<FontAwesomeIconProvider>();

            AvaloniaXamlLoader.Load(this);
        }

        public override void OnFrameworkInitializationCompleted()
        {
            Locator.CurrentMutable.Register(() => new MainView(), typeof(IViewFor<MainViewModel>));

            Locator.CurrentMutable.Register(() => new MachinesPage(), typeof(IViewFor<MachinesViewModel>));
            Locator.CurrentMutable.Register(() => new ProfilePage(), typeof(IViewFor<ProfilViewModel>));
            Locator.CurrentMutable.Register(() => new ServersPage(), typeof(IViewFor<ServersViewModel>));
            Locator.CurrentMutable.Register(() => new SettingsPage(), typeof(IViewFor<SettingViewModel>));
            Locator.CurrentMutable.Register(() => new UsersPage(), typeof(IViewFor<UsersViewModel>));
            
            if (ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                desktop.MainWindow = new MainWindow
                {
                    DataContext = Locator.Current.GetService<MainViewModel>()
            };
            }
            else if (ApplicationLifetime is ISingleViewApplicationLifetime singleViewPlatform)
            {
                singleViewPlatform.MainView = new MainView
                {
                    DataContext = Locator.Current.GetService<MainViewModel>()
                };
            }

            base.OnFrameworkInitializationCompleted();
        }
    }
}
